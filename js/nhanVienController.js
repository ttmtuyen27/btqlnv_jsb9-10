function layThongTinTuForm() {
  var tk = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var mk = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucvu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  if (chucvu == 1) var chucVuValue = "Nhân viên";
  else if (chucvu == 2) var chucVuValue = "Sếp";
  else if (chucvu == 3) var chucVuValue = "Trưởng phòng";
  var newNhanVien = new NhanVien(
    tk,
    ten,
    email,
    mk,
    ngayLam,
    luongCB,
    chucVuValue,
    gioLam
  );
  return newNhanVien;
}

function xuatThongTin(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var nhanVien = dsnv[i];
    var contentTrTag = "";
    contentTrTag = `
      <tr>
        <td>${nhanVien.taiKhoanNv}</td>
        <td>${nhanVien.tenNv}</td>
        <td>${nhanVien.emailNv}</td>
        <td>${nhanVien.ngayLamNv}</td>
        <td>${nhanVien.chucVuNv}</td>
        <td>${nhanVien.tinhTongLuong()}</td>
        <td>${nhanVien.xepLoaiNhanVien()}</td>
        <td class="px-2">
          <button class="btn btn-danger p-1 m-0" onclick="xoaNhanVien(${
            nhanVien.taiKhoanNv
          })">
            Xóa
          </button>
          <button class="btn btn-success p-1 m-0" 
          onclick="xuatThongTinLenForm(${nhanVien.taiKhoanNv})">
            Sửa
          </button>
        </td>
      </tr>
    `;
    contentHTML += contentTrTag;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
