function NhanVien(
  _taiKhoan,
  _ten,
  _email,
  _matKhau,
  _ngayLam,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoanNv = _taiKhoan;
  this.tenNv = _ten;
  this.emailNv = _email;
  this.matKhauNv = _matKhau;
  this.ngayLamNv = _ngayLam;
  this.luongCoBanNv = _luongCoBan;
  this.chucVuNv = _chucVu;
  this.gioLamNv = _gioLam;
  this.tinhTongLuong = function () {
    if (this.chucVuNv == "Sếp") return this.luongCoBanNv * 3;
    else if (this.chucVuNv == "Trưởng phòng") return this.luongCoBanNv * 2;
    else return this.luongCoBanNv * 1;
  };
  this.xepLoaiNhanVien = function () {
    if (this.gioLamNv >= 192) return "Xuất sắc";
    else if (this.gioLamNv >= 176) return "Giỏi";
    else if (this.gioLamNv >= 160) return "Khá";
    else return "Trung bình";
  };
}
