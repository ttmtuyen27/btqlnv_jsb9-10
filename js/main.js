var danhSachNhanVien = [];
var validator = new ValidatorNv();
const DANHSACHNHANVIEN_LOCALSTORAGE = "DANHSACHNHANVIEN_LOCALSTORAGE";

const luuLocalStorage = function () {
  var dsnvJson = JSON.stringify(danhSachNhanVien);
  localStorage.setItem(DANHSACHNHANVIEN_LOCALSTORAGE, dsnvJson);
};

dsnvJson = localStorage.getItem(DANHSACHNHANVIEN_LOCALSTORAGE);
if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);
  danhSachNhanVien = danhSachNhanVien.map(function (item) {
    return new NhanVien(
      item.taiKhoanNv,
      item.tenNv,
      item.emailNv,
      item.matKhauNv,
      item.ngayLamNv,
      item.luongCoBanNv,
      item.chucVuNv,
      item.gioLamNv
    );
  });
  xuatThongTin(danhSachNhanVien);
}

//*Validate input for add function
valiadateInputInAddFunction = () => {
  var isValid = true;
  var isValidTaiKhoan =
    validator.kiemTraTaiKhoanHopLe() && validator.kiemTraTaiKhoanTrung();
  var isValidTen =
    validator.kiemTraTenHopLe() &&
    validator.kiemTraRong("name", "tbTen", "Tên không được để trống");
  var isValidEmail = validator.kiemTraEmailHopLe();
  var isValidDate = validator.kiemTraDateHopLe();
  var isValidPassword = validator.kiemTraMatKhauHopLe();
  var isValidLuongCB = validator.kiemTraLuongHopLe();
  var isValidChucVu = validator.kiemTraChucVuHopLe();
  var isvalidGioLam = validator.kiemTraGioLamHopLe();
  isValid =
    isValidTaiKhoan &&
    isValidTen &&
    isValidEmail &&
    isValidDate &&
    isValidPassword &&
    isValidLuongCB &&
    isValidChucVu &&
    isvalidGioLam;
  return isValid;
};

//*Validate input for edit function
valiadateInputInEditFunction = () => {
  var isValid = true;
  var isValidTen =
    validator.kiemTraTenHopLe() &&
    validator.kiemTraRong("name", "tbTen", "Tên không được để trống");
  var isValidEmail = validator.kiemTraEmailHopLe();
  var isValidDate = validator.kiemTraDateHopLe();
  var isValidPassword = validator.kiemTraMatKhauHopLe();
  var isValidLuongCB = validator.kiemTraLuongHopLe();
  var isValidChucVu = validator.kiemTraChucVuHopLe();
  var isvalidGioLam = validator.kiemTraGioLamHopLe();
  isValid =
    isValidTen &&
    isValidEmail &&
    isValidDate &&
    isValidPassword &&
    isValidLuongCB &&
    isValidChucVu &&
    isvalidGioLam;
  return isValid;
};

$(".modal").on("hidden.bs.modal", function () {
  $(this).find("form")[0].reset();
});

//*Reset form when click "Thêm Sinh Viên" button
document.getElementById("btnThem").addEventListener("click", () => {
  document.getElementById("tknv").disabled = false;
  $(".modal").on("hidden.bs.modal", function () {
    $(this).find("form")[0].reset();
  });
  document.getElementById("tbTKNV").innerText = "";
  document.getElementById("tbTen").innerText = "";
  document.getElementById("tbEmail").innerText = "";
  document.getElementById("tbMatKhau").innerText = "";
  document.getElementById("tbNgay").innerText = "";
  document.getElementById("tbLuongCB").innerText = "";
  document.getElementById("tbChucVu").innerText = "";
  document.getElementById("tbGiolam").innerText = "";
});

document.getElementById("btnDong").addEventListener("click", () => {
  $(".modal").on("hidden.bs.modal", function () {
    $(this).find("form")[0].reset();
  });
});

//*Add employees function
document.getElementById("btnThemNV").addEventListener("click", function () {
  var newNhanVien = layThongTinTuForm();
  let isValid = valiadateInputInAddFunction();
  if (isValid) {
    $("#myModal").modal("hide");
    danhSachNhanVien.push(newNhanVien);
    xuatThongTin(danhSachNhanVien);
  }
  luuLocalStorage();
  $(".modal").on("hidden.bs.modal", function () {
    $(this).find("form")[0].reset();
  });
});

//*Find index
findPosition = (id) => {
  return danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoanNv == id;
  });
};

xoaNhanVien = (id) => {
  let index = findPosition(id);
  danhSachNhanVien.splice(index, 1);
  luuLocalStorage();
  xuatThongTin(danhSachNhanVien);
};

xuatThongTinLenForm = (id) => {
  $("#myModal").modal("show");
  let index = findPosition(id);
  let nhanVien = danhSachNhanVien[index];
  document.getElementById("tknv").value = nhanVien.taiKhoanNv;
  document.getElementById("tknv").disabled = true;
  document.getElementById("name").value = nhanVien.tenNv;
  document.getElementById("email").value = nhanVien.emailNv;
  document.getElementById("password").value = nhanVien.matKhauNv;
  document.getElementById("datepicker").value = nhanVien.ngayLamNv;
  document.getElementById("luongCB").value = nhanVien.luongCoBanNv;
  document.getElementById("chucvu").value =
    nhanVien.chucVuNv == "Nhân viên"
      ? "1"
      : nhanVien.chucVuNv == "Sếp"
      ? "2"
      : "3";
  document.getElementById("gioLam").value = nhanVien.gioLamNv;
};

//*Edit employees function
document.getElementById("btnCapNhat").addEventListener("click", () => {
  let nhanVienEdit = layThongTinTuForm();
  let isValid = valiadateInputInEditFunction();
  if (isValid) {
    $("#myModal").modal("hide");
    let index = findPosition(nhanVienEdit.taiKhoanNv);
    danhSachNhanVien[index] = nhanVienEdit;
    luuLocalStorage();
    xuatThongTin(danhSachNhanVien);
  }
  $(".modal").on("hidden.bs.modal", function () {
    $(this).find("form")[0].reset();
  });
});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

document.getElementById("btnTimNV").addEventListener("click", () => {
  let searchValue = document.getElementById("searchName").value;
  searchValue = capitalizeFirstLetter(searchValue);
  let danhSachCanTim = danhSachNhanVien.filter(function (item) {
    return item.xepLoaiNhanVien() == searchValue;
  });
  xuatThongTin(danhSachCanTim);
});
