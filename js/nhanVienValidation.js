function ValidatorNv() {
  this.kiemTraRong = function (idTarget, idError, message) {
    var targetValue = document.getElementById(idTarget).value;
    if (!targetValue) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraTaiKhoanHopLe = function () {
    var taiKhoanValue = document.getElementById("tknv").value;
    var pattern = /^[0-9]{1,6}$/;
    if (!pattern.test(taiKhoanValue)) {
      document.getElementById("tbTKNV").innerText = "Tài khoản không hợp lệ";
      document.getElementById("tbTKNV").style.display = "block";
      return false;
    } else {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    }
  };

  this.kiemTraTenHopLe = function () {
    var tenValue = document.getElementById("name").value;
    var pattern = /^[a-zA-Z\s]*$/;
    if (!pattern.test(tenValue)) {
      document.getElementById("tbTen").innerText = "Tên không hợp lệ";
      document.getElementById("tbTen").style.display = "block";
      return false;
    } else {
      document.getElementById("tbTen").innerText = "";
      return true;
    }
  };

  this.kiemTraEmailHopLe = function () {
    var emailValue = document.getElementById("email").value;
    var pattern =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(emailValue)) {
      document.getElementById("tbEmail").innerText = "Email không hợp lệ";
      document.getElementById("tbEmail").style.display = "block";
      return false;
    } else {
      document.getElementById("tbEmail").innerText = "";
      return true;
    }
  };

  this.kiemTraDateHopLe = function () {
    var dateValue = document.getElementById("datepicker").value;
    var pattern =
      /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;
    if (!pattern.test(dateValue)) {
      document.getElementById("tbNgay").innerText = "Ngày không hợp lệ";
      document.getElementById("tbNgay").style.display = "block";
      return false;
    } else {
      document.getElementById("tbNgay").innerText = "";
      return true;
    }
  };

  this.kiemTraMatKhauHopLe = function () {
    var passValue = document.getElementById("password").value;
    var pattern = /^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/;
    if (!pattern.test(passValue)) {
      document.getElementById("tbMatKhau").innerText = "Mật khẩu không hợp lệ";
      document.getElementById("tbMatKhau").style.display = "block";
      return false;
    } else {
      document.getElementById("tbMatKhau").innerText = "";
      return true;
    }
  };

  this.kiemTraLuongHopLe = function () {
    var luongValue = document.getElementById("luongCB").value * 1;
    if (!(luongValue >= 1000000 && luongValue <= 20000000)) {
      document.getElementById("tbLuongCB").innerText = "Lương không hợp lệ";
      document.getElementById("tbLuongCB").style.display = "block";
      return false;
    } else {
      document.getElementById("tbLuongCB").innerText = "";
      return true;
    }
  };

  this.kiemTraChucVuHopLe = function () {
    var chucVuValue = document.getElementById("chucvu").value * 1;
    if (chucVuValue == 0) {
      document.getElementById("tbChucVu").innerText = "Chức vụ không hợp lệ";
      document.getElementById("tbChucVu").style.display = "block";
      return false;
    } else {
      document.getElementById("tbChucVu").innerText = "";
      return true;
    }
  };

  this.kiemTraGioLamHopLe = function () {
    var gioLamValue = document.getElementById("gioLam").value * 1;
    if (!(80 <= gioLamValue && gioLamValue <= 200)) {
      document.getElementById("tbGiolam").innerText = "Số giờ làm không hợp lệ";
      document.getElementById("tbGiolam").style.display = "block";
      return false;
    } else {
      document.getElementById("tbGiolam").innerText = "";
      return true;
    }
  };

  this.kiemTraTaiKhoanTrung = function () {
    let taiKhoanValue = document.getElementById("tknv").value;
    let index = danhSachNhanVien.findIndex(function (item) {
      return item.taiKhoanNv == taiKhoanValue;
    });
    if (index != -1) {
      document.getElementById("tbTKNV").innerText = "Tài khoản đã bị trùng";
      document.getElementById("tbTKNV").style.display = "block";
      return false;
    } else {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    }
  };
}
